let express = require('express')
let app = express()
let hbs = require('express-hbs')
let path = require('path')
let port = 9800

app.use('/', express.static(path.join(__dirname, '/dist')))

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/src/app.html')
})

app.listen(port, function () {
  console.log(`Server up on port ${port}!`)
})