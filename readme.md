ForceStore App
--------------
To have this application up and running in localhost, follow these steps:

1. Run `git clone https://git@bitbucket.org/elfleat/forcestore.git`
2. Run `yarn` (or `npm install`) in app root folder
3. Run `npm run compile`
4. Run `npm run serve`
5. Open http://localhost:9800/ on your web browser

Feel free to get in touch:
contact@eliassierra.pro
http://eliassierra.pro

--------

TODO:

 - Add unit testing
 - Integrate css autoprefixer on Webpack config
 - Move all colors from scss files to _variables.scss file
 - Add a filter view to control the main menu as a grid filter
 - Add shopping cart view to reflect the contents of the cart: DONE
