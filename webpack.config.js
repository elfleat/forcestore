let path = require('path')
let webpack = require('webpack')
let ExtractTextPlugin = require('extract-text-webpack-plugin')
let BrowserSyncPlugin = require('browser-sync-webpack-plugin')
let bourbon = require('bourbon')

module.exports = {
  entry: path.resolve(__dirname, './src/app.js'),
  devtool: 'sourcemap',
  output: {
    filename: './dist/js/bundle.js'
  },
  resolve: {
    alias: {
        $: "jquery/src/jquery"
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      Backbone: "backbone",
      _: "underscore",
      hbs: "handlebars"
    }),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 3000
    })
  ],
  module: {
    rules: [
      {
        test: /\.(sass|scss)$/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader?url=false" // translates CSS into CommonJS
        }, {
            loader: "sass-loader", // compiles Sass to CSS
            options: {
              includePaths: [bourbon.includePaths]
            }
        }]
      },
      { 
        test: /\.hbs$/, 
        loader: "handlebars-loader" 
      }
    ]
  }
}