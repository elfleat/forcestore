// Bring the CSS!
require('./scss/app.scss');
// I know, not a fan of inline css either,
// but since I'm not using http2, it will help
// to keep the http requests low. Bare with me.

// Models
var StateModel = require('./models/app.state');

// Collections
var ProductsCollection = require('./collections/products');

// Services
var BingService = require('./services/bing.images');
var YoutubeService = require('./services/youtube.videos');

// Views
var ProductsGrid = require('./views/products.grid'); 
var ProductDetails = require('./views/product.details');
var Menu = require('./views/menu');

// Declare our Router Routes.  
var Router = require('./app.router'); 

// This view will be used as our App Controller.
var ForceStoreApp = Backbone.View.extend({
    initialize: function() {
        // Declare views object on App instance, to keep track of what we instantiate.
        this.views = {};

        // Set up some default settings to avoid hardcoding them just anywhere
        var settings = {
            dataFeed: 'http://demo7475333.mockable.io/spaceships',
            bingApi: '3ffe71dde3a5433aa302abb1ec1e07bd'
        };

        // Create instance of App Router
        this.router = new Router();

        // Create instance of our main Model and Collection
        this.state = new StateModel(settings, this);
        this.products = new ProductsCollection({}, this);

        // Create instance of Services
        this.bingService = new BingService({ parent: this });
        this.youtubeService = new YoutubeService({ parent: this });

        // Create instance of global views
        this.menu = new Menu({
            parent: this,
            el: this.$('[data-menu]')
        });

        // Moving the State Events binding to a separate
        // method for readability purposes.
        this.bindStateEvents();

        Backbone.history.start();
    },
    bindStateEvents: function() {
        // Binding stuff only once
        // (For more complex OPAs I'd also manage
        // the unbinding of the events)
        this.state.on('change:loading', this.switchLoadingState, this);
        this.state.on('change:activePage', this.switchPage, this);
    },
    switchLoadingState: function() {
        // Based on the current state of the app, add or 
        // remove -.is-loading- class from app container.
        if(this.state.get('loading')) {
            this.$el.addClass('is-loading');
        } else {
            this.$el.removeClass('is-loading');
        }
    },
    switchPage: function() {
        var targetPage = this.state.get('activePage');

        // Create grid view if it doesn't exist already.
        // This way we avoid ghost views (unused bindings).
        this.views.productsGrid = !this.views.productsGrid ? new ProductsGrid({
            el: this.$('[data-module="products-grid"]'),
            collection: this.products,
            parent: this
        }) : this.views.productsGrid;

        if(targetPage == 'home' && this.views.productDetails) {
            this.views.productDetails.close();
        }

        if(targetPage == 'productDetails') {

            if(this.views.productDetails) {
                this.views.productDetails.open();
            } else {
                this.views.productDetails = new ProductDetails({
                    el: this.$('[data-module="product-details"]'),
                    parent: this
                });
            }

        }
    }
})

// Create an instance of our App. 
new ForceStoreApp({ el: $('body') }); 