var BingImages = Backbone.Model.extend({
    initialize: function(args) {
        this.settings = args.parent.settings;
        
    },
    findOnCache: function(param) {
        param = JSON.stringify(param);

        // If record not found on cache, return a dead flag.
        var cachedResponse = sessionStorage.getItem(param) || false;

        // If record found, convert into JSON object.
        return cachedResponse ? JSON.parse(cachedResponse) : false;
    },
    saveOnCache: function(param, response) {
        // Store queries using stringified parameters as unique key.
        sessionStorage.setItem(JSON.stringify(param), JSON.stringify(response));
        return this;
    },
    getImages: function(params, callback) {
        var defaultParams = {
            'q': 'star wars',
            'mkt': "en-US",
            'imageType': 'Photo',
            'aspect': 'Square',
            'safeSearch': 'Strict'
        };

        // If params provided, extend defaults with it.
        params = params ? _.extend(defaultParams, params) : defaultParams;

        if(_.isObject(this.findOnCache(params))) {
            // If we find the params in the cache,
            // we save the http request and happily
            // execute the callback right away.
            return callback(this.findOnCache(params));
        }

        $.ajax({
            url: "https://api.cognitive.microsoft.com/bing/v5.0/images/search",
            beforeSend: function(xhrObj){
                // Request headers
                xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key","3ffe71dde3a5433aa302abb1ec1e07bd");
            },
            type: "GET", 
            data: params,
        })
        .done(function(data) {

            // Extract only the data that we need from the response.
            var images = this.extractImages(data.value);

            // Grab request and pass it into the our
            // local cache.
            this.saveOnCache(params, images);

            // Do whatever you intended 
            // with your data.
            callback(images); 
        }.bind(this))
        .fail(function() {

            // TODO: Build fail state.
            console.error(arguments);
        });
    },
    extractImages: function(rawImages) {

        // No need to store more data in memory
        // than the one we actually need.
        var output = {
            mainImage: null,
            additionalImages: []
        };

        _.each(rawImages, function(rawImage) {
            output.additionalImages.push(rawImage.thumbnailUrl)
        });

        // Making sure the first image won't appear twice.
        output.mainImage = output.additionalImages[0];
        delete output.additionalImages[0];

        return output;
    }
});

module.exports = BingImages;