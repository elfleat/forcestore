var YoutubeVideos = Backbone.Model.extend({
    initialize: function(args) {
        this.settings = args.parent.settings;
        
    },
    findOnCache: function(param) {
        param = JSON.stringify(param);

        // If record not found on cache, return a dead flag.
        var cachedResponse = sessionStorage.getItem(param) || false;

        // If record found, convert into JSON object.
        return cachedResponse ? JSON.parse(cachedResponse) : false;
    },
    saveOnCache: function(param, response) {
        // Store queries using stringified parameters as unique key.
        sessionStorage.setItem(JSON.stringify(param), JSON.stringify(response));
        return this;
    },
    getVideo: function(params, callback) {
        var defaultParams = {
            part: 'snippet',
            key: 'AIzaSyDAKDaBy_JDwcScSHqDQimOOLjdPImLanc',
            q: 'star wars'
        };

        // If params provided, extend defaults with it.
        params = params ? _.extend(defaultParams, params) : defaultParams;

        if(_.isObject(this.findOnCache(params))) {
            // If we find the params in the cache,
            // we save the http request and happily
            // execute the callback right away.
            return callback(this.findOnCache(params));
        }

        $.ajax({
            url: "https://www.googleapis.com/youtube/v3/search",
            beforeSend: function(xhrObj){
                // Request headers
                xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key","3ffe71dde3a5433aa302abb1ec1e07bd");
            },
            type: "GET", 
            data: params,
        })
        .done(function(data) {

            // Extract only the data that we need from the response.
            var videoUrl = this.exportVideoUrl(data.items);

            // Grab request and pass it into the our
            // local cache.
            this.saveOnCache(params, videoUrl);

            // Do whatever you intended 
            // with your data.
            callback(videoUrl); 
        }.bind(this))
        .fail(function() {

            // TODO: Build fail state.
            console.error(arguments);
        });
    },
    exportVideoUrl: function(entries) {

        var output = {
            source: 'https://www.youtube.com/embed/' + entries[0].id.videoId,
            thumbnail: entries[0].snippet.thumbnails.default.url
        }

        return output;
    }
});

module.exports = YoutubeVideos;