
var GalleryItem = Backbone.Model.extend({
    defaults: {
        'isVideo': false,
        'source': '//placeholdit.imgix.net/~text?txtsize=33&txt=&w=50&h=50',
        'isActive': false
    },
    initialize: function() {
        // Set Model unique ID as property
        // to be used to target specific items
        this.set('id', this.cid);
    }
});

module.exports = GalleryItem;