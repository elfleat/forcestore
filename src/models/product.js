// Our model for each of our ships.
var ProductModel = Backbone.Model.extend({
    initialize: function() {
        this.generateId()
            .checkPrice()
            .sortSpecsByName()
            .generatePermalink();
    },
    generatePermalink: function() {
        var id = this.get('id'),
            appUrl = window.location.origin;

        return this.set('permalink', appUrl + '#product/' + id);
    },
    checkPrice: function() {
        if(!this.get('price')) {
            this.set({
                'noPrice': true,
                'price': 'N/A'
            });
        }
        return this;
    },
    // Sorting the technical specs alphabetically.
    // IDEA: To store an -importance score- to be used
    //       to sort, that way we ensure that more 
    //       eye-catching specs are always displayed.
    sortSpecsByName: function() {
        var specs = this.get('techspecs');
        var keys = _.sortBy(_.keys(specs), function(a) { return a.toLowerCase(); });
        var orderedSpecs = {};

        _.each(keys, function(k) {
            orderedSpecs[k] = specs[k];
        });

        return this.set('techspecs', orderedSpecs);;
    },
    generateId: function() {
        var name = this.get('name');

        // We only want to set unique ids if
        // model has data.
        if(!name) return this;

        // Set unique Id that could be
        // reversed engineered by replacing
        // blank spaces with - and setting to lowercase.
        this.set('id', name.toLowerCase().replace(/ /gi, '-'));

        return this;
    },
    // Created separate export methods to make sure we only
    // expose to respective modules the data that is needed.
    // No more. No less. Better performance and more Security.
    getBasicDetails: function() {
        var output = {
            'name'  : this.get('name'),
            'id'    : this.get('id'),
            'price' : this.get('price')
        };

        return output;
    },
    getDetails: function() {
        var output = this.toJSON();

        // Making a couple of specs more friendly
        // This would be more dynamic if the application
        // supported multiple languages.
        if(_.has(output.techspecs, 'maxatmosphericspeed')) {
            output.techspecs['Maximum Atmospheric Speed'] = output.techspecs.maxatmosphericspeed;
            delete output.techspecs.maxatmosphericspeed;
        }

        if(_.has(output.techspecs, 'maxaccel')) {
            output.techspecs['Maximum Acceleration'] = output.techspecs.maxaccel;
            delete output.techspecs.maxaccel;
        }

        return output;
    }
});

module.exports = ProductModel;