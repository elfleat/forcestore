// Current Model will contain the State of the App.
var StateModel = Backbone.Model.extend({
    defaults: {
        loading: true,
        activePage: null // [ "home" | "productDetails" ]
    },
    initialize: function(n, parent) {
        this.router = parent.router;

        this.router.on('route:home', this.switchPage, this);
        this.router.on('route:productDetails', this.switchPage, this);

        this.on('products-loaded', this.setReadyState, this);
    },
    switchPage: function(hash) {
        if(!hash) {
            // If no hash is passed in the url, it's safe to assume
            // that the home page is being requested => Update State Model.
            return this.set('activePage', 'home');
        }

        // TODO: CHECK IF PRODUCT EXISTS
        this.set('activeProduct', hash);
        return this.set('activePage', 'productDetails');
    },
    addToCart: function(prodId) {
        var cart = this.get('cart') || [];
        cart.push(prodId);
        this.set('cart', cart);
        return this.trigger('cart-change');
    },
    countCartItems: function() {
        var cart = this.get('cart') || [];
        return cart.length;
    },
    setReadyState: function() {
        this.set('loading', false);
    }
});

module.exports = StateModel;