var ProductModel = require('./../models/product');

// Products Collection
var Products = Backbone.Collection.extend({
    model: ProductModel,
    initialize: function(n, parent) {

        // Store locally the App state instance
        this.state = parent.state;
        
        // Added a setTimeout for DEV shocase Only
        // I just want to make sure that you won't miss out
        // the App Loading State Screen!
        setTimeout(function() {
            this.getProducts();
        }.bind(this), 750)
    },
    getProducts: function() {

        // Use the endpoint provided in the assignment PDF
        // to retrieve data.
        $.get(this.state.get('dataFeed'), function(res){
            this.reset(res.products);
            this.state.trigger('products-loaded');
        }.bind(this));

        // TODO:
        // Create a Fail State for the App.
    },
    gridExport: function() {
        // This method exports only the data that the
        // grid needs out of each product, instead of 
        // exposing all the data. Better performance, More secure.
        var output = [];

        this.each(function(product) {
            output.push(product.getBasicDetails())
        });

        return output;
    },
    extract: function(attribute) {
        // This method should return all the unique
        // values of an specific attribute on each model.
        return this.pluck(attribute);
    }
});

module.exports = Products;