var GalleryItem =  require('./../models/gallery.item');

var GalleryCollection = Backbone.Collection.extend({
    model: GalleryItem,
    resetActive: function() {
        // Find active item and deactivate.
        this.find({isActive: true}).set('isActive', false);
        return this;
    },
    switchActive: function(targetId) {
        // First we grab current active item
        var currentActive = this.find({ isActive: true });
        
        // If our target is our current active, escape
        if(currentActive.cid == targetId) return this;

        // Disable state for current active
        // As silent to avoid re-render of gallery on this change.
        currentActive.set('isActive', false, {silent: true});

        // Enable state for target
        this.find({id: targetId}).set('isActive', true);
    },
    checkForActive: function() {
        if(!this.length) return this;

        // If none of the items are active,
        // activate first one.
        if(!this.find({isActive: true})) {
            this.first().set('isActive', true);
        }
        return this;
    },
    export: function() {
        var output = {
            activeItem: this.find({isActive: true}).toJSON(),
            allItems: this.toJSON()
        }
        return output;
    }
})

module.exports = GalleryCollection;