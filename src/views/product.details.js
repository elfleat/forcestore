var Gallery = require('./gallery');

var ProductDetails = Backbone.View.extend({
    events: {
        'click [data-close]': 'close',
        'click [data-add-to-cart]': 'addToCart',
        'click [data-collapse-specs]': 'toggleSpecs'
    },
    initialize: function (args) {
        this.parent = args.parent;
        this.state = args.parent.state;
        this.products = args.parent.products;
        this.router = args.parent.router;
        this.template = require('./../templates/product-details.hbs');

        if(!this.state.get('loading')) {
            this.open();
        } else {
            this.state.on('products-loaded', this.open.bind(this));
        }
    },
    render: function(activeProduct) {
        // Pass in product data to template and 
        // populate DOM container with the output.
        this.$el.html(this.template(activeProduct.getDetails()));

        if(this.gallery) {
            // If there is currently an instance of a Gallery,
            // remove it from memory to avoid ghost views.
            this.gallery.unbind();
            delete this.gallery;
        }

        // Create instance of Gallery
        this.gallery = new Gallery({ el: this.$('[data-gallery]'), parent: this.parent });

        // Making the class to be async to ensure
        // it runs after and not at the same time as the
        // module population. #AnimationPurposes #WhyNot
        setTimeout(function(){
            // Happy state class for styling freedom.
            this.$el.addClass('is-active');
        }.bind(this), 50);

        return this;
    },
    open: function() {
        // Check if there is a product selected.
        var activeProductId = this.state.get('activeProduct'),
            activeProduct = activeProductId ? this.products.find({id : activeProductId }) : false;

        // If couldn't find selected product, redirect to home.
        if(!activeProduct) {
            return this.router.navigate('/', { trigger: true })
        }

        return this.render(activeProduct);
    },
    close: function(e) {
        // Instead of stopping the propagation
        // simple check to not do anything if the
        // clicked element doesn't has the data attribute.
        if(e && !_.has(e.target.dataset, 'close')) {
            return;
        }

        // Update our state class
        this.$el.removeClass('is-active');

        // Switch back to Homepage.
        this.router.navigate('/', { trigger: true });

        // Returns instance of itself to allow chaining
        // for future integrations with other modules
        return this;
    },
    toggleSpecs: function() {
        // To toggle we first need to find the current collapser
        // and find out its current state.
        var collapser = this.$('[data-specs-collapser]'),
            isCollapsed = collapser.hasClass('is-collapsed') ? true : false;

        // Then we act accordingly.
        if(isCollapsed) {
            collapser.removeClass('is-collapsed');
        } else {
            collapser.addClass('is-collapsed');
        }

        return this;
    },
    addToCart: function(e) {
        var prodId = e.currentTarget.dataset.addToCart;
        console.log('added', prodId)
        this.state.addToCart(prodId);
    }
});

module.exports = ProductDetails;