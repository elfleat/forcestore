
var Placeholder = Backbone.View.extend({
    initialize: function(args) {

        // Store the bingService instance from the parent view.
        this.bingService = args.parent.bingService;

        // Load content on initialize.
        this.loadContent();
    },
    loadContent: function() {

        // Use the service api to pass-in the dynamic query based on the data attribute.
        // Set local method as callback, ensuring the current scope for execution.
        this.bingService.getImages({q: this.$el.data('placeholder')}, this.updateContent.bind(this));
    },
    updateContent: function(images) {
        this.$el.removeClass('is-loading');
        this.$('img').attr('src', images.mainImage);
    }
});

module.exports = Placeholder; 