var GalleryTemplate = require('./../templates/gallery.hbs');

var GalleryCollection = require('./../collections/gallery');

var Gallery = Backbone.View.extend({
    events: {
        'click [data-gallery-switch]': 'switchItem',
        'click [data-gallery-expand]': 'expandGallery'
    },
    initialize: function(args) {
        this.bingService = args.parent.bingService;
        this.youtubeService = args.parent.youtubeService;

        this.productId = this.$el.data('id');
        this.template = GalleryTemplate;

        this.collection = new GalleryCollection();
        
        // Render on initialize.
        this.collection.on('change', this.render.bind(this));

        this.populateGallery();
    },
    render: function() {
        this.$el.html(this.template(this.collection.export()));
    },
    switchItem: function(e){
        var target = e.target.dataset.gallerySwitch;
        this.collection.switchActive(target);

        return this;
    },
    expandGallery: function() {
        this.$el.toggleClass('is-expanded');
    },
    populateGallery: function() {
        // Retreive images and add the first 3 in the collection.
        this.bingService.getImages({q: this.$el.data('gallery')}, function(images) {

            var selection = [];

            selection.push({ source: images.mainImage });

            _.each(_.first(_.compact(images.additionalImages), 2), function(image) {
                selection.push({
                    source: image
                });
            }.bind(this));

            this.collection.add(selection);
            this.collection.checkForActive();
        }.bind(this));

        // Retreive images and add the first 3 in the collection.
        this.youtubeService.getVideo({q: this.$el.data('gallery')}, function(video) {

            this.collection.add({ 
                source: video.source,
                thumbnail: video.thumbnail,
                isVideo: true
            });

            this.render();
            
        }.bind(this));
    }
})

module.exports = Gallery;