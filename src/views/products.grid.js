var ProductsGridTemplate = require('./../templates/products-grid.hbs');

var Placeholder = require('./placeholder')

var ProductsGrid = Backbone.View.extend({
    events: {
        'click a': 'redirect'
    },
    initialize: function(args) {
        this.parent = args.parent;

        // Store template locally. Encapsulation purposes.
        this.template = ProductsGridTemplate;

        // Bind to the StateApp to render once products get available.
        this.parent.state.on('products-loaded', this.render, this);
    },
    redirect: function(e) {

        // Use the appRouter once any link in the grid gets clicked.
        var target = $(e.currentTarget).attr('href');
        this.parent.router.navigate(target, {trigger: true});
        
        return this;
    },
    render: function() { 

        // Populate it's DOM container with the output of the 
        // template using the current data.
        this.$el.html(this.template(this.collection.gridExport()));

        // A happy event for whoever wants to listen.
        this.trigger('rendered');

        return this.initPlaceholders();
    },
    initPlaceholders: function() {

        // Create an instance of Placeholder for each DOM element
        // with its selector.
        this.$('[data-placeholder]').each(function(i, el) {
            // Adding an incremental delay for 2 reasons.
            // 1. I really want the users to notice the
            //    loading states (for demo purposes, of course).
            // 2. I'm using a free bing api to do the image searches,
            //    so I'm trying to not to get banned for too many
            //    simultaneous requests.
            // PLUS: The sequential loading of images is kind of cool.
            setTimeout(function(){
                new Placeholder({ el: el, parent: this.parent });
            }.bind(this), 150*i);
        }.bind(this));

        return this;
    }
});

module.exports = ProductsGrid;