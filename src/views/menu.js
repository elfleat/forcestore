var MenuTemplate = require('./../templates/menu.hbs');

var Menu = Backbone.View.extend({
    initialize: function(args) {
        this.parent = args.parent;
        this.template = MenuTemplate;

        this.parent.state.on('products-loaded', this.render, this); 
        this.parent.state.on('cart-change', this.render, this);    
    },
    render: function() {
        var cartCount = this.parent.state.countCartItems();
        var menuItems = {
            manufacturer: _.uniq(this.parent.products.extract('manufacturer')),
            class: _.uniq(this.parent.products.extract('class')),
            cartItems: this.parent.state.countCartItems()
        }

        this.$el.html(this.template(menuItems));

        if(cartCount) {
            this.$('[data-cart]').addClass('highlight');

            setTimeout(function(){
                this.$('[data-cart]').removeClass('highlight');
            }.bind(this), 400);
        }

        return this;
    }
});

module.exports = Menu;