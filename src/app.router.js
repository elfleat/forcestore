var Router = Backbone.Router.extend({
    routes: {
        'product/:id': 'productDetails',
        '*any': 'home'
    }
});

module.exports = Router;